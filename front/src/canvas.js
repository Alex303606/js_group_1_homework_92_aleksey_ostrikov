import React, { Component } from 'react';
import axios from './axios-api';

class Canvas extends Component {
	
	isPainting = false;
	userStrokeStyle = '#ff0000';
	line = [];
	prevPos = {offsetX: 0, offsetY: 0};
	
	onMouseDown = ({nativeEvent}) => {
		const {offsetX, offsetY} = nativeEvent;
		this.isPainting = true;
		this.prevPos = {offsetX, offsetY};
	}
	
	onMouseMove = ({nativeEvent}) => {
		if (this.isPainting) {
			const {offsetX, offsetY} = nativeEvent;
			const offSetData = {offsetX, offsetY};
			const positionData = {
				start: {...this.prevPos},
				stop: {...offSetData},
			};
			this.line = this.line.concat(positionData);
			this.paint(this.prevPos, offSetData, this.userStrokeStyle);
		}
	};
	
	endPaintEvent = () => {
		if (this.isPainting) {
			this.isPainting = false;
			this.sendPaintData();
		}
	}
	
	paint = (prevPos, currPos, strokeStyle) => {
		const {offsetX, offsetY} = currPos;
		const {offsetX: x, offsetY: y} = prevPos;
		this.ctx.beginPath();
		this.ctx.strokeStyle = strokeStyle;
		this.ctx.moveTo(x, y);
		this.ctx.lineTo(offsetX, offsetY);
		this.ctx.stroke();
		this.prevPos = {offsetX, offsetY};
	}
	
	sendPaintData = () => {
		const body = {line: this.line};
		this.websocket.send(JSON.stringify(body));
	}
	
	componentDidMount() {
		this.canvas.width = 800;
		this.canvas.height = 600;
		this.ctx = this.canvas.getContext('2d');
		this.ctx.lineJoin = 'round';
		this.ctx.lineCap = 'round';
		this.ctx.lineWidth = 5;
		
		axios.get('/paint').then(
			response => {
				response.data.line.forEach((position) => {
					this.paint(position.start, position.stop, this.userStrokeStyle);
				});
			}
		);
		
		this.websocket = new WebSocket('ws://localhost:8000/paint');
		
		this.websocket.onmessage = (message) => {
			const pixelsArray = JSON.parse(message.data);
			pixelsArray.line.forEach((position) => {
				this.paint(position.start, position.stop, this.userStrokeStyle);
			});
		};
	}
	
	render() {
		return (
			<canvas
				ref={(ref) => (this.canvas = ref)}
				style={{background: 'black'}}
				onMouseDown={this.onMouseDown}
				onMouseLeave={this.endPaintEvent}
				onMouseUp={this.endPaintEvent}
				onMouseMove={this.onMouseMove}
			/>
		);
	}
}

export default Canvas;
