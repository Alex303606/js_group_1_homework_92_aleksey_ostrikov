const express = require('express');
const cors = require('cors');
const app = express();
const expressWs = require('express-ws')(app);
const mongoose = require('mongoose');
const config = require('./config');
const Paint = require('./models/Paint');
const paint = require('./app/paint');

const port = 8000;

app.use(cors());

const clients = {};

app.ws('/paint', (ws, req) => {
	const id = req.get('sec-websocket-key');
	clients[id] = ws;
	console.log('Client connected.');
	
	ws.on('message', (msg) => {
		try {
			const image = new Paint(JSON.parse(msg));
			image.save().then(
				Object.values(clients).forEach(client => {
					client.send(msg)
				}));
		} catch (error) {
			console.log(error);
		}
	});
	
	ws.on('close', () => {
		delete clients[id];
		console.log('Client disconnected.');
	});
});

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
	console.log('Mongoose connected!');
	app.use('/paint', paint());
	app.listen(port, () => {
		console.log(`Server started on ${port} port!`);
	});
});
