const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PaintSchema = new Schema({
	line: {
		type: Array,
		required: true,
	}
});

const Paint = mongoose.model('Paint', PaintSchema);

module.exports = Paint;
